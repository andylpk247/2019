---
title: PyGotham 2019 is Six Months Away!
date: 2019-04-01 00:00:00 -0400
---

PyGotham is back for 2019.

[Tickets]({{ site.data.event.registration_url }}) are available now. Get your early bird tickets
while they last.

The [Call for Proposals]({{ site.data.event.cfp_url }}) is open until
May 12th, 2019 [Anywhere on Earth](https://time.is/Anywhere_on_Earth).

Sponsorship information can be found on the
[sponsorship prospectus page]({% link sponsors/prospectus.md %}).
