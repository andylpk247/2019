---
name: Annie Cook
talks:
- "Convincing an entire engineering org to use (and like) mypy "
---
Annie is a Senior Software Engineer at Nylas. She works almost exclusively
in Python to build out new features for the Nylas API. Outside of work,
Annie teaches computer science at an all girls high school. She is
passionate about making complicated topics accessible and helping others
grow as engineers.
