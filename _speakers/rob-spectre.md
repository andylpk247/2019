---
name: Rob Spectre
talks:
- "Using NLP To Reduce Wealth Flowing Into Local Human Trafficking Economies"
---
Rob Spectre is a Brooklyn-based technologist building childsafe.ai, the
artificial intelligence platform protecting kids from predators online.
Before founding childsafe.ai, Rob served teams at software startups like
Boxee and Twilio and worked for a year as a technical consultant for counter
human trafficking units across the United States. Learning from the wisdom
and experience of those law enforcement partners, Rob creates machine
learning software that starves the macroeconomy fueling modern slavery of
its wealth, anonymity and distribution.

A programmer by trade and a historian by training, Rob also serves youth
organizations HackNY and DoSomething.org as an advisor, plays in a punk rock
band and is the loudest Red Sox fan in New York.
