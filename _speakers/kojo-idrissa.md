---
name: Kojo Idrissa
talks:
- "The Python Use Spectrum"
---
Kojo Idrissa *was* an accountant who got an MBA and taught at university in
two different countries. He's *now* a  software engineer, international tech
conference speaker, DjangoCon US organizer and the
[DEFNA](https://www.defna.org/) North American Ambassador
([#NorAmGT](https://twitter.com/search?q=%23NorAmGT&src=tyah)). He's spoken
at tech conferences about software engineering practices, spreadsheets,
contributing to tech communities, Dungeons & Dragons, inclusion and
privilege. You can find him online at http://kojoidrissa.com/ or as
[@transition](https://twitter.com/Transition) on Twitter.
