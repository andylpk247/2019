---
name: Ryan Kelly
talks:
- "Advanced SQL with SQLAlchemy"
---
Ryan Kelly is CTO of Capital Rx, the future of pharmacy benefits. With over
a decade of experience in software development, security, and analytics,
Ryan has the skills required for the modern healthcare startup. He recently
served as the software architect for Truveris, managing the design,
implementation, delivery for all products.
